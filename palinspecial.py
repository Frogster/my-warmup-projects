#Find the largest palindrome made from the product of two 3-digit numbers.
def palindrome(input):
    return str(input) == str(input)[::-1]

#program starts here
lower = 100
top = 999
large_palindrome = 0
for i in range (top, lower,-1):
    for j in range( top, lower,-1):
        product  = i*j
        if palindrome(product):
            if product > large_palindrome:
                large_palindrome = product
print large_palindrome


