#What is the largest prime factor of the number 600851475143 ?
range = 'enter any number\n'
num = input(range)        #here we can write 600851475143 
i = 2
while i * i < num:
     while num % i == 0:
         num = num / i
     i = i + 1

print "Answer is: \t"
print (num)

